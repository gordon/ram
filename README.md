# Rust AUR Manager
Or in short, ram
## Whatisthis
An AUR Helper written in Rust.
## Usage
There are two forms.
### Searching the AUR
```ram supertux```

This will list all AUR packages which names contain "supertux", each associated with a number. You will be promted now for a white-space separated list of package numbers.

### Using as package manager
```ram -Syu```

When given pacman-like options to, ram will just call pacman to fulfill your request.

AUR upgrades are done in the following way:

```ram -Syua```