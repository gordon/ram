use crate::error::AurError;
use crate::install;
use crate::pacman;
use crate::search;
use std::path::Path;
use std::process::Command;

fn git_clone(url: &str) -> std::io::Result<()> {
    let _exitcode = Command::new("git").args(&["clone", url]).status()?;
    Ok(())
}

fn get_revision() -> std::io::Result<String> {
    let out = Command::new("git")
        .args(&["rev-parse", "HEAD"])
        .output()?
        .stdout;

    Ok(String::from_utf8_lossy(&out).trim().to_string())
}

fn git_diff(rev: &str) -> std::io::Result<()> {
    let _exitcode = Command::new("git").args(&["diff", rev]).status()?;
    Ok(())
}

fn git_pull() -> std::io::Result<()> {
    let _exitcode = Command::new("git").arg("pull").status()?;
    Ok(())
}

pub fn upgrade_pkg(pkg: &str, local_version: String) -> std::result::Result<(), AurError> {
    if let Some(aur_version) = search::available(pkg)? {
        if aur_version > local_version {
            println!(
                "AUR package: {} {} installed, {} upstream",
                pkg, local_version, aur_version
            );

            let home = std::env::var("HOME").or(Err("Home directory could not be found"))?;
            let pkgpath = Path::new(&home).join(".ram").join(pkg);

            // If not there, clone the AUR repo of the package
            if !pkgpath.exists() {
                let p = Path::new(&home).join(".ram");
                std::env::set_current_dir(&p)?;
                git_clone(&format!("https://aur.archlinux.org/{0}.git", pkg))?;
            }
            std::env::set_current_dir(&pkgpath)?;

            let old_rev = &get_revision()?;

            git_pull()?;
            let new_rev = &get_revision()?;
            if old_rev != new_rev {
                git_diff(old_rev)?;
            }
            install::edit_pkgbuild(pkg);
            install::install_dependencies(pkg)?;
            install::build_and_install(pkg)?;
        }
    }
    Ok(())
}

pub fn upgrade() -> std::result::Result<(), AurError> {
    let mut unsuccessful = false;
    for (pkg, local_version) in pacman::foreign()? {
        println!("Checking upgrade for {}", pkg);
        if let Err(e) = upgrade_pkg(&pkg, local_version) {
            unsuccessful = true;
            println!(
                "An error occured while checking '{}' upgrades:  {:?}",
                pkg, e
            );
        }
    }

    if unsuccessful {
        Err(AurError::from(
            "Not all packages could be checked for upgrades.",
        ))
    } else {
        Ok(())
    }
}
