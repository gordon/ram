use serde_json::error::Category;
use std::error;
use std::fmt;

#[derive(Debug)]
pub enum AurError {
    ProcessErr(std::io::Error),
    JsonErr(serde_json::Error),
    Arbitrary(String),
}

impl error::Error for AurError {
    fn description(&self) -> &str {
        match self {
            AurError::ProcessErr(_) => "Process error",
            AurError::JsonErr(_) => "JSON error while using the AUR",
            AurError::Arbitrary(s) => s,
        }
    }
    
    fn cause(&self) -> Option<&dyn error::Error> {
        match self {
            AurError::ProcessErr(e) => e.source(),
            AurError::JsonErr(e) => e.source(),
            AurError::Arbitrary(_) => Some(self),
        }
    }
}

impl fmt::Display for AurError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AurError::ProcessErr(e) => write!(f, "{}", e),
            AurError::JsonErr(e) => match e.classify() {
                Category::Io => write!(f, "{}", "failure to read or write bytes on an IO stream"),
                Category::Syntax => write!(f, "not syntactically valid JSON"),
                Category::Data => write!(f, "semantically incorrect at line {}", e.line()),
                Category::Eof => write!(f, "unexpected end of the input data"),
            },
            AurError::Arbitrary(s) => write!(f, "{}", s),
        }
    }
}

impl From<std::io::Error> for AurError {
    fn from(error: std::io::Error) -> AurError {
        AurError::ProcessErr(error)
    }
}

impl From<&str> for AurError {
    fn from(error: &str) -> AurError {
        AurError::Arbitrary(error.to_string())
    }
}

impl From<reqwest::Error> for AurError {
    fn from(error: reqwest::Error) -> AurError {
        AurError::Arbitrary(error.to_string())
    }
}
