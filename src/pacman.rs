use regex::Regex;
use std::io::ErrorKind;
use std::process::{Command, Stdio};
use std::str;

pub fn pacman(args: &[String]) -> std::io::Result<()> {
    let mut args = args.to_vec();
    args.insert(0, String::from("pacman"));
    Command::new("sudo")
        .args(args)
        .stdin(Stdio::inherit())
        .stderr(Stdio::inherit())
        .stdout(Stdio::inherit())
        .output()?;
    Ok(())
}

pub fn install(pkgs: &[String]) -> std::io::Result<()> {
    let mut args = pkgs.to_vec();
    args.insert(0, String::from("-S"));
    pacman(&args)
}

// Checks with `pacman -Q <pkg>` if pkg is installed.
pub fn installed(pkg: &str) -> std::io::Result<bool> {
    Ok(Command::new("pacman")
        .args(&["-Q", pkg])
        .output()?
        .status
        .success())
}

// Checks if a package is installable via pacman.
// The check is done via `pacman -Ss <package>`.
pub fn available(package: &str) -> Option<String> {
    lazy_static! {
        // We want to match against strings like "community/package 0.6.0-2 [Installed]", which can be the output of `pacman -Ss package`
        static ref RE: Regex = Regex::new(r"^([\w/]+)\s").unwrap();
    }

    match Command::new("pacman")
        .args(&["--color", "never", "-Ss", &format!("^{}$", &package)])
        .output()
    {
        Err(_) => None,
        Ok(output) => {
            if !output.status.success() {
                return None;
            }

            let output = match str::from_utf8(&output.stdout) {
                Ok(s) => s,
                Err(_) => return None,
            };

            match RE.captures(output) {
                None => None,
                // If the following is true, the package appeared in the search result.
                Some(caps) => Some(String::from(caps.iter().next().unwrap().unwrap().as_str())),
            }
        }
    }
}

// Generates a Vec of (package_name, version) for all foreign packages.
// Foreign packages are the packages potentially provided through AUR.
pub fn foreign() -> std::io::Result<Vec<(String, String)>> {
    let output = Command::new("pacman").arg("-Qm").output()?.stdout;
    match str::from_utf8(&output) {
        Ok(output) => Ok(output
            .lines()
            .map(|line| {
                let mut line = line.split_whitespace();
                (
                    line.next().unwrap_or("").to_string(),
                    line.next().unwrap_or("").to_string(),
                )
            })
            .collect()),
        Err(e) => return Err(std::io::Error::new(ErrorKind::Other, e)),
    }
}
