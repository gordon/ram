use std::fmt;

#[derive(Serialize, Deserialize, Debug)]
pub struct Results {
    pub version: u8,
    pub resultcount: u32,
    pub results: Vec<Package>,
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Package {
    pub Name: String,
    //    pub PackageBaseID: u32,
    //    pub PackageBase: String,
    pub Version: String,
    pub Description: Option<String>,
    pub URL: Option<String>,
    pub NumVotes: Option<u32>,
    pub OutOfDate: Option<u64>,
    pub Maintainer: Option<String>,
    //    pub FirstSubmitted: u32, /*time str*/
    //    pub LastModified: u32,   /*time str*/
    //    pub URLPath: String,
}

impl std::fmt::Display for Package {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\n	Version: {}\n", self.Name, self.Version)?;
        if let Some(description) = &self.Description {
            write!(f, "	Description: {}\n", description)?;
        }
        if let Some(url) = &self.URL {
            write!(f, "	URL: {}\n", url)?;
        }
        if let Some(num_votes) = &self.NumVotes {
            write!(f, "	NumVotes: {}\n", num_votes)?;
        }
        if let Some(maintainer) = &self.Maintainer {
            write!(f, "	Maintainer: {}\n", maintainer)?;
        }
        if let Some(out_of_date) = &self.OutOfDate {
            write!(f, "	OutOfDate: {}\n", out_of_date)?;
        }
        Ok(())
    }
}

pub fn readline() -> std::io::Result<String> {
    let mut line = String::new();
    std::io::stdin().read_line(&mut line)?;
    return Ok(line);
}

pub fn pkg_list(list: &Vec<Package>) -> String {
    let mut it = list.iter();
    if let Some(s) = it.next() {
        let mut acc = s.Name.clone();
        for element in it {
            acc.push(' ');
            acc.push_str(&element.Name);
        }
        acc
    } else {
        String::new()
    }
}
