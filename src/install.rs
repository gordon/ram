use crate::error::AurError;
use crate::pacman;
use std::io::*;
use std::process::{Command, Stdio};
use std::str;

pub fn clone(package: &str) -> std::io::Result<std::process::ExitStatus> {
    Command::new("sh")
        .args(&["-c", &format!("if test -d $HOME/.ram/{0}; then cd $HOME/.ram/{0}; git pull; else mkdir -p $HOME/.ram; cd $HOME/.ram; git clone https://aur.archlinux.org/{0}.git; fi", package)])
        .status()
}

pub fn edit_pkgbuild(pkg: &str) -> bool {
    print!(
        "Do you want to edit the PKGBUILD file for {}? [y/N]? > ",
        pkg
    );
    std::io::stdout().flush().unwrap();
    let choice = crate::base::readline().unwrap();

    if choice == "y\n" || choice == "Y\n" {
        Command::new("sh")
            .args(&[
                "-c",
                &format!("${{EDITOR:-nano}} $HOME/.ram/{}/PKGBUILD", pkg),
            ])
            .stdin(Stdio::inherit())
            .stderr(Stdio::inherit())
            .stdout(Stdio::inherit())
            .output()
            .is_ok()
    } else {
        false
    }
}

pub fn get_dependencies(package: &str) -> std::result::Result<Vec<String>, AurError> {
    match str::from_utf8(
        &Command::new("bash")
            .args(&[
                "-c",
                &format!(
                    "cd $HOME/.ram/{}; . PKGBUILD; echo -n ${{depends[@]}}",
                    package
                ),
            ])
            .output()?
            .stdout,
    ) {
        Ok(s) => Ok(s
            .split_whitespace()
            .map(|x| String::from(x))
            .filter(|pkg| pkg.len() > 0)
            .filter(|pkg| !pacman::installed(pkg).unwrap_or(false))
            .collect()),
        Err(e) => Err(AurError::Arbitrary(format!("{}", e))),
    }
}

pub fn install_dependencies(package: &str) -> std::result::Result<(), AurError> {
    let dep_list = get_dependencies(package)?;
    let mut pacman_list = vec![];
    let mut aur_list = vec![];
    let visual_list: Vec<_> = dep_list
        .iter()
        .map(|pkg| {
            if let Some(pkg) = pacman::available(&pkg) {
                pacman_list.push(pkg.clone());
                pkg
            } else {
                aur_list.push(pkg);
                format!("{} (AUR)", pkg)
            }
        })
        .collect();

    if visual_list.len() > 0 {
        println!(
            "Will install the following dependencies for {}: {:?}",
            package, visual_list
        );
        for &pkg in aur_list.iter() {
            clone(pkg)?;
            edit_pkgbuild(pkg);
            install_dependencies(pkg)?;
            build_and_install(pkg)?;
        }

        pacman::install(&pacman_list)?;
    }
    Ok(())
}

pub fn build_and_install(package: &str) -> std::io::Result<std::process::ExitStatus> {
    Command::new("sh")
        .args(&["-c", &format!("cd $HOME/.ram/{}; makepkg -Csi", package)])
        .status()
}
