use std::env;

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;
extern crate regex;

mod base;
mod error;
mod install;
mod pacman;
mod search;
mod upgrade;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() > 1 {
        // If the argument starts with '-', it must be an argument for pacman.
        if args[1].starts_with('-') {
            let mut args: Vec<String> = env::args().skip(1).collect();

            if args[0].contains('S') && args[0].contains('a') {
                args[0] = args[0].chars().filter(|c| c != &'a').collect();
                pacman::pacman(&args).expect("Something went wrong while calling pacman.");
                upgrade::upgrade().unwrap();
            } else {
                pacman::pacman(&args[0..]).expect("Something went wrong while calling pacman.");
            }
        } else {
            // Was wollt ihr dann? A U R !!
            match search::run() {
                Some(packages) => {
                    println!(
                        "The following packages are going to be installed:  {}",
                        base::pkg_list(&packages)
                    );
                    for package in &packages {
                        if let Ok(_) = install::clone(&package.Name) {
                            install::install_dependencies(&package.Name).unwrap();
                            install::edit_pkgbuild(&package.Name);
                            install::build_and_install(&package.Name).unwrap();
                        }
                    }
                }
                None => println!("Nothing to do"),
            }
        }
    } else {
        println!("Arguments?")
    }
}
