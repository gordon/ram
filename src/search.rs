use crate::base::Package;
use crate::base::Results;
use crate::error::AurError;
use std::io::Write;
use std::str;
extern crate reqwest;

pub fn http_get(url: &str) -> reqwest::Result<String> {
    Ok(reqwest::get(url)?.text()?)
}

pub fn query(name: &str) -> Result<Results, AurError> {
    let data = http_get(&format!(
        "https://aur.archlinux.org/rpc/?v=5&type=search&arg={}",
        name
    ))?;
    Ok(serde_json::from_str(&data).map_err(AurError::JsonErr)?)
}

pub fn available(pkg: &str) -> Result<Option<String>, AurError> {
    let results = query(pkg)?;
    Ok(results
        .results
        .iter()
        .filter(|package| &package.Name == pkg)
        .map(|package| package.Version.clone())
        .next())
}
pub fn readline() -> std::io::Result<String> {
    let mut line = String::new();
    std::io::stdin().read_line(&mut line)?;
    return Ok(line);
}

pub fn strtoints(line: &str) -> Result<Vec<usize>, std::num::ParseIntError> {
    let mut list = vec![];
    for res in line.split_whitespace().map(|s| s.parse::<usize>()) {
        match res {
            Ok(n) => list.push(n),
            Err(e) => return Err(e),
        }
    }
    return Ok(list);
}

pub fn run() -> Option<Vec<Package>> {
    match std::env::args().nth(1) {
        None => {
            print!("Argument, please?");
            None
        }
        Some(arg) => match query(&arg) {
            Err(e) => {
                print!("Error while contacting AUR: {}\n", e);
                None
            }
            Ok(results) => {
                // Present the search results
                for (i, package) in results.results.iter().enumerate() {
                    println!("{}. {}", i, package);
                }

                let choice = if results.results.len() > 1 {
                    print!(" Your input => ");
                    std::io::stdout().flush().unwrap();
                    let input = readline().unwrap();
                    strtoints(&input)
                } else {
                    Ok(vec![0])
                };

                match choice {
                    Ok(packages) => Some(
                        results
                            .results
                            .iter()
                            .enumerate()
                            .filter(|(i, _p)| packages.iter().any(|x| x == i))
                            .map(|(_i, p)| p.clone())
                            .collect(),
                    ),
                    Err(_) => None,
                }
            }
        },
    }
}
